FROM node:carbon

ARG VERSION=dev

# ------------------------
# SSH Server support
# ------------------------
RUN apt-get update \
    && apt-get install -y --no-install-recommends openssh-server \
        && echo "root:Docker!" | chpasswd

COPY sshd_config /etc/ssh/
COPY init_container.sh /bin/
RUN chmod 755 /bin/init_container.sh

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Install app dependencies
COPY package.json /usr/src/app/
RUN npm install

# Bundle app source
COPY . /usr/src/app
RUN echo $VERSION > /usr/src/app/VERSION

EXPOSE 8080 2222
CMD /bin/init_container.sh && \
    export VERSION=`cat /usr/src/app/VERSION` && \
    echo $VERSION && \
    node ./bin/www