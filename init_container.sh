#!/usr/bin/env bash
cat >/etc/motd <<EOL

|￣￣￣￣￣￣￣￣￣￣￣￣￣|
|    Welcome to SSH     |
|＿＿＿＿＿＿＿＿＿＿＿＿＿|

EOL
cat /etc/motd

service ssh start